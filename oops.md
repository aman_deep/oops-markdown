# object oriented programming (oops)
## INTRODUCTION
- Object-Oriented Programming (**OOPs**) is a **programming paradigm** that relies on the concept of `classes and objects`. 
- Everything in **OOPs** is associated with `classes and objects`, along with its attributes and methods. 
- For example: in real life, a car is an `object`. The car has attributes, such as weight and color, and methods, such as drive and brake. `Class` is used to structure a software program into **simple** and **reusable** pieces of code blueprints, which are used to create individual instances of `objects`. There are many _**OOPs**_ languages including _Ruby, JavaScript, C++, Java_, and _Python_, etc.

## OOPS CONCEPT
There are basically **six** concepts of **OOPs**, are given below:
- Class
- Object
- Polymorphism
- Inheritance
- Encapsulation
- Abstraction

## STRUCTURAL DESIGN

![intellipaat Image](https://intellipaat.com/mediaFiles/2018/12/j4-1.png)

## CLASS AND OBJECT

- An object-oriented programming language involves `classes and objects`. 
- A `class` is the **blueprint** from which individual `objects` are created and defines the variables and the methods common to all objects of a certain kind.

```
EXAMPLES

# defining class Car 
class Car
    # defining method 
    def brands
        puts "BMW"
        puts "Audi"
    end
end
# creating object 
obj=Car.new
# calling method using object 
obj.brands

```
output

```
BMW
Audi

```

## Polymorphism -

`Polymorphism` is a very important concept in programming. It refers to the use of a single type entity (method, operator or object) to represent different types in different scenarios.

here are two types of **Polymorphism**-
- **Static Polymorphism**
  - linking of a function with an `object` during *compile-time*.
  - also known as **Static Binding**.
  - _Method Overloading_ and _Operator Overloading_ are the examples of **Static Polymorphism**.
- **Dynamic Polymorphism**
  - linking of a function with an `object` during _run-time_.
  - also known as **Run-time Polymorphism**.
  - Method Overriding and Duck Typing are examples of **Dynamic Polymorphism**.

## Inheritance -
In an object-oriented programming language, `Inheritance` is one of the most important features. It allows us to inherit the characteristics of one class into another class. It provides the concept of Reusability. It is a **mechanism** where you can derive a `class` from another `class` for a hierarchy of classes that share a set of attributes and methods.
There are basically the following types of `Inheritance` given below:
- Single Inheritance
- Multiple Inheritance
- Multi-level Inheritance
- Hierarchical Inheritance
- Hybrid Inheritance

```
Python Inheritance Syntax
 
 class BaseClass:
  Body of base class
class DerivedClass(BaseClass):
  Body of derived class

```

![Inhertitance](https://intellipaat.com/wp-content/uploads/2015/09/inheritance-types.png)


  
## Encapsulation -
`Encapsulation` is defined as the wrapping up of data under a single unit. It is the mechanism that binds together **data** and **methods**. It describes the idea of bundling **data** and **methods** that work within one unit.

## Abstraction -
The idea of representing **significant details** and hiding details of functionality is called data `Abstraction`. It is a concept of hiding the complexities of a system from the users of that system.


# References
|Referencs | Links
|------------ | -------------|
|Reference 1 | https://www.w3schools.com/python/trypython.asp?filename=demo_class2|
|Reference 2 | https://www.programiz.com/python-programming/polymorphism|
|Reference 3 | https://www.javatpoint.com/ruby-tutorial|







